import datetime
import json
import random
import time
import uuid

from wzl import mqtt

MQTT_USER = ""
MQTT_PASSWORD = ""

MQTT_BROKER = "127.0.0.1"
MQTT_VHOST = "/"
WEBSOCKET = False
SSL = True

topic = "test"  # set topic to publish according to MQTT syntax!

if __name__ == "__main__":
    client = mqtt.MQTTPublisher()
    client.connect(MQTT_BROKER, MQTT_USER, MQTT_PASSWORD, vhost=MQTT_VHOST, ssl=SSL, websocket=WEBSOCKET)

    while True:
        try:
            message = json.dumps({"value": [random.uniform(0, 5) for i in range(3)], "timestamp": datetime.datetime.utcnow().isoformat() + "Z",
                                  "covariance": [[2, 0, 0], [0, 2, 0], [0, 0, 0]], "nonce": str(uuid.uuid4()), "hash": None, "unit": "MTR"})
            client.publish(topic, message.encode("utf-8"), 0)
            time.sleep(1)
        except KeyboardInterrupt:
            break
