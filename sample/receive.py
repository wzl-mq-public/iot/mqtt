import time

from wzl import mqtt

MQTT_USER = ""
MQTT_PASSWORD = ""

MQTT_BROKER = "127.0.0.1"
MQTT_VHOST = "/"
WEBSOCKET = False
SSL = True

topic = "test"  # set topic to subscribe according to MQTT syntax!
qos = 0  # set QoS according to MQTT specifications!


def print_mqtt_message(topic, message):
    print("### {} ###\r\n{}\r\n".format(topic, message.decode("utf-8")))


if __name__ == "__main__":
    client = mqtt.MQTTSubscriber()
    client.connect(MQTT_BROKER, MQTT_USER, MQTT_PASSWORD, vhost=MQTT_VHOST, ssl=SSL, websocket=WEBSOCKET)

    client.set_callback("PRINT", print_mqtt_message)
    client.subscribe(topic, qos)

    while True:
        try:
            time.sleep(1)
        except KeyboardInterrupt:
            break
