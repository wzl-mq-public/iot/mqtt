import time

from src import mqtt

# logger = mqtt.root_logger.get('Receiver')

### Ask for settings and individual credentials ###

MQTT_USER = ""
MQTT_PASSWORD = ""

MQTT_BROKER = "mqtt.wzl-mq.rwth-aachen.de"
MQTT_PORT = 8883
MQTT_VHOST = "metrology"

### Ask for settings and individual credentials ###

topic = "#" # set topic to subscribe according to MQTT syntax!
qos = 0 # set QoS according to MQTT specifications!

def print_mqtt_message(topic, message):
    print("### {} ###\r\n{}\r\n".format(topic, message.decode("utf-8")))

if __name__=="__main__":
    client = mqtt.MQTTSubscriber()
    client.connect(MQTT_BROKER, MQTT_USER, MQTT_PASSWORD, vhost=MQTT_VHOST, ssl=True, port=MQTT_PORT)

    client.set_callback("PRINT", print_mqtt_message)
    # client.subscribe(topic, qos)

    while True:
        try:
            time.sleep(1)
        except KeyboardInterrupt:
            break

