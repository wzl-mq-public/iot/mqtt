import datetime
import json
import random
import time
import uuid

from src import mqtt

### Ask for settings and individual credentials ###

MQTT_USER = ""
MQTT_PASSWORD = ""

MQTT_BROKER = "mqtt.wzl-mq.rwth-aachen.de"
MQTT_PORT = 8883
MQTT_VHOST = "metrology"

### to connect to the central MQTT-Broker of MQ-MS use the following settings:
# MQTT_BROKER = "wzl-mbroker01.wzl.rwth-aachen.de"
# MQTT_PORT = 1883
# MQTT_VHOST = "metrology"

### Ask for settings and individual credentials ###


if __name__ == "__main__":
    client = mqtt.MQTTPublisher()
    client.connect(MQTT_BROKER, MQTT_USER, MQTT_PASSWORD, vhost=MQTT_VHOST, port=MQTT_PORT, ssl=True)

    while True:
        try:
            message = json.dumps({"value": [random.uniform(0, 5) for i in range(3)], "timestamp": datetime.datetime.utcnow().isoformat() + "Z",
                                  "covariance": [[2, 0, 0], [0, 2, 0], [0, 0, 0]], "nonce": str(uuid.uuid4()), "hash": None, "unit": "MTR"})
            client.publish("channel-001", message.encode("utf-8"), 2)
            # time.sleep(0.01)
        except KeyboardInterrupt:
            break
