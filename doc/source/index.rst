.. WZL-MQTT documentation master file, created by
   sphinx-quickstart on Wed Mar 17 14:15:45 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to WZL-MQTT's documentation!
====================================

Installation
------------

For installation refer to the README of the Repository in GitLab-CE.

..
   Install the WZL-MQTT package via pip

   .. code-block:: bat

      pip install --extra-index-url https://package-read:gkYP4xrm2PxicUbW1wra@git-ce.rwth-aachen.de/api/v4/projects/1708/packages/pypi/simple wzl-mqtt


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   usage
   mqtt



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
